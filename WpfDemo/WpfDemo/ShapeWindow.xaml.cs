﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfDemo
{
    /// <summary>
    /// ShapeWindow.xaml 的交互逻辑
    /// </summary>
    public partial class ShapeWindow : Window
    {
        public ShapeWindow()
        {
            InitializeComponent();
        }

        private void btn1_Click(object sender, RoutedEventArgs e)
        {
            Line l1 = new Line();
            l1.X1 = 0;
            l1.Y1 = 0;
            l1.X2 = 100;
            l1.Y2 = 100;
            l1.Stroke = new SolidColorBrush(Color.FromRgb(0, 0, 255));
            l1.StrokeThickness = 2;

            grid1.Children.Add(l1);
        }

        private double oldX, oldY;

        /// <summary>
        /// 鼠标点击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                oldX = e.GetPosition(this).X;
                oldY = e.GetPosition(this).Y;
            }
        }

        /// <summary>
        /// 鼠标移动事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            if(e.LeftButton == MouseButtonState.Pressed)
            {
                var x = e.GetPosition(this).X;
                var y = e.GetPosition(this).Y;

                var dx = x - oldX;
                var dy = y - oldY;
                this.Left += dx;
                this.Top += dy;

                oldX = x;
                oldY = y;
            }
        }
    }
}
